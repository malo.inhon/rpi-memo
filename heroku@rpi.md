## 在Heroku要Login的方法

使用帳號是申請的email，但密碼不是你登入網頁的密碼，而是在Account頁面中的API Key，只要輸入API Key就可以正常登入。如果輸入網頁密碼，會得到 `Error ID: unauthorized`的錯誤

```

pi@raspberrypi:~/my-heroku$ heroku login
heroku: Enter your login credentials
Email [malo.inhon@gmail.com]:
Password: ************************************
Logged in as malo.inhon@gmail.com

```

- 本來一直登入不了，是[受到這一篇的啟發的](https://devcenter.heroku.com/articles/authentication)，雖然他也還沒有很清楚的指明…
